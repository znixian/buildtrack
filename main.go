package main

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"os"
)

var globalBuilds *BuildCollection

type SiteInfo struct {
	ProjectName string
	UrlPrefix   string

	ListenProto string // Use "unix" for UNIX sockets
	ListenAddr  string

	RepoPath string

	// SHA-256 hash of the upload secret
	UploadSecretHash string
}

var siteInfo *SiteInfo

func main() {
	loadSiteConfig()

	var err error
	globalBuilds, err = NewBuildCollection()
	if err != nil {
		log.Panicf("Failed to create builds collection: %s", err)
	}

	http.HandleFunc("/webhook/upload_build", webhookUploadBuild)
	http.HandleFunc("/view_build", handleShowBuild)
	http.HandleFunc("/download_build", handleDownloadBuild)
	http.HandleFunc("/", handleRoot)
	log.Println("Running HTTP server")

	if siteInfo.ListenProto == "unix" {
		_ = os.Remove(siteInfo.ListenAddr)
	}

	listener, err := net.Listen(siteInfo.ListenProto, siteInfo.ListenAddr)
	if err != nil {
		log.Fatalf("Listening on UNIX socket failed: %s", err)
	}

	if siteInfo.ListenProto == "unix" {
		err = os.Chmod(siteInfo.ListenAddr, 0666)
		if err != nil {
			log.Fatalf("Failed to set socket permissions: %s", err)
		}
	}

	err = http.Serve(listener, nil)
	if err != nil {
		log.Fatalf("Server failed: %s", err)
	}
}

func loadSiteConfig() {
	configData, err := os.ReadFile("configBuildTrack.json")
	if err != nil {
		log.Fatalf("Could not read config: %s", err)
	}

	siteInfo = &SiteInfo{}
	err = json.Unmarshal(configData, siteInfo)
	if err != nil {
		log.Fatalf("Could not parse config: %s", err)
	}
}
