package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

func handleShowBuild(w http.ResponseWriter, req *http.Request) {
	var err error

	repo, err := getCachedRepo()
	if err != nil {
		log.Printf("Failed to load cached repo for handleShowBuild: %s", err)
		w.WriteHeader(500)
		return
	}

	query := req.URL.Query()
	commit := repo.ById[query.Get("commit")]

	if commit == nil {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("No such commit."))
		return
	}

	// Reload on serve for debugging
	rootTemplate := template.Must(template.ParseFiles("template/build.html"))

	type DisplayArtefact struct {
		Available bool
		Name      string
		Size      string
		JobName   string
		UniqueID  string
	}

	type BuildDisplayInfo struct {
		Info      *BuildInfo
		BRI       *BuildReadInfo
		Artefacts []*DisplayArtefact
	}

	var ctx struct {
		SiteInfo *SiteInfo
		C        *Commit
		Builds   []*BuildDisplayInfo
	}
	ctx.SiteInfo = siteInfo
	ctx.C = commit

	for _, build := range globalBuilds.ByCommit[commit.Hash] {
		disp := &BuildDisplayInfo{
			Info: build.Info,
			BRI:  build,
		}
		ctx.Builds = append(ctx.Builds, disp)

		// Join the AppVeyor artefacts with that in the BuildInfo struct
		displayArtefacts := make(map[string]*DisplayArtefact)
		for _, artefact := range build.Info.Artefacts {
			seekInfo := build.Files[artefact.ArchiveFilename]

			da := &DisplayArtefact{
				Name:     artefact.AppveyorFilename,
				UniqueID: artefact.UniqueID,
			}
			disp.Artefacts = append(disp.Artefacts, da)
			displayArtefacts[artefact.JobId+"/"+artefact.AppveyorFilename] = da

			// If seekInfo==nil then the file is missing from the archive, for example
			// if the download failed.
			if seekInfo != nil {
				da.Available = true
				if seekInfo.Size < 1.5*1024*1024 {
					da.Size = fmt.Sprintf("%.2f KiB", float32(seekInfo.Size)/1024)
				} else {
					da.Size = fmt.Sprintf("%.2f MiB", float32(seekInfo.Size)/1024/1024)
				}
			}

		}

		for _, job := range build.AppVeyor.EventData.Jobs {
			for _, artefact := range job.Artefacts {
				da := displayArtefacts[job.Id+"/"+artefact.FileName]
				da.JobName = job.Name
				if da.JobName == "" {
					da.JobName = "Unnamed"
				}
			}
		}

	}

	err = rootTemplate.Execute(w, &ctx)
	if err != nil {
		log.Printf("Failed to render build template: %s", err)
		return
	}
}

func handleDownloadBuild(w http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()
	commitId := query.Get("commit")
	buildId := query.Get("build_id")
	artefactId := query.Get("artefact_id")

	builds := globalBuilds.ByCommit[commitId]

	var build *BuildReadInfo
	for _, candiate := range builds {
		if strconv.Itoa(candiate.Info.BuildId) == buildId {
			build = candiate
			break
		}
	}

	if build == nil {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("No such commit/build_id pair"))
		return
	}

	// Find the filename
	var artefact *Artefact
	for _, candidate := range build.Info.Artefacts {
		if candidate.UniqueID == artefactId {
			artefact = candidate
		}
	}
	if artefact == nil {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("No such artefact"))
		return
	}

	// Find the file info
	seekInfo := build.Files[artefact.ArchiveFilename]
	if seekInfo == nil {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("The requested artefact is missing from the build file"))
		return
	}

	// Open the file and seek to the correct position - do as much as possible before
	// writing the headers, so we can still send error codes.
	file, err := os.Open(build.ArchivePath)
	if err != nil {
		w.WriteHeader(500)
		log.Printf("Failed to open file '%s' for downloading artefact %s.%s.%s: %s",
			build.ArchivePath, commitId, buildId, artefactId, err)
		return
	}

	_, err = file.Seek(seekInfo.StartOffset, io.SeekStart)
	if err != nil {
		w.WriteHeader(500)
		log.Printf("Failed to seek in file '%s' for artefact '%s': %s",
			build.ArchivePath, artefact.ArchiveFilename, err)
		return
	}

	// Write the headers for the download
	w.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", artefact.AppveyorFilename))
	w.Header().Add("Content-Length", strconv.FormatInt(seekInfo.Size, 10))
	w.WriteHeader(200)

	// Copy the content to the browser
	_, err = io.CopyN(w, file, seekInfo.Size)
	if err != nil {
		// Try and make it obvious (to the extent we can) to the client
		_, _ = w.Write([]byte("**** BUILD SERVER: TRUNCATED *****"))
		log.Printf("Failed to fully download file '%s' artefact '%s': %s",
			build.ArchivePath, artefact.AppveyorFilename, err)
		return
	}
}
