package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/blakesmith/ar"
)

const BUILDS_DIR = "builds"

const APPVEYOR_TIMESTAMP_FORMAT = "2/1/2006 3:4 PM"

func loadBuilds() {
}

// Our representation of the JSON structure that we get sent by AppVeyor
type AppveyorNotification struct {
	EventName string `json:"eventName"`

	EventData struct {
		RepositoryProvider string `json:"repositoryProvider"`
		RepositoryName     string `json:"repositoryName"`
		Branch             string `json:"branch"`

		BuildURL string `json:"buildUrl"`

		ProjectName string `json:"projectName"`
		ProjectId   int    `json:"projectId"`
		BuildId     int    `json:"buildId"`
		BuildNumber int    `json:"buildNumber"`

		CommitId string `json:"commitId"`

		Started  string `json:"started"`
		Finished string `json:"finished"`

		Jobs []struct {
			Id     string `json:"id"`
			Status string `json:"status"`
			Name   string `json:"name"`

			Artefacts []struct {
				Permalink string `json:"permalink"`
				FileName  string `json:"fileName"`
				Name      string `json:"Name"`
				URL       string `json:"URL"`
			} `json:"artifacts"`
		} `json:"jobs"`
	} `json:"eventData"`
}

// The custom data block that we write
type BuildInfo struct {
	BuildId    int
	Date       time.Time
	Commit     string // The full commit ID - EventData contains a truncated version
	Successful bool
	Artefacts  []*Artefact
}

type Artefact struct {
	JobId            string
	AppveyorFilename string
	ArchiveFilename  string
	UniqueID         string // A random, unique ID that we generate
}

func webhookUploadBuild(w http.ResponseWriter, req *http.Request) {
	var err error

	// Authenticate the request
	rawKey := req.Header.Get("X-BuildTrack-Auth")
	hashedKeyBytes := sha256.Sum256([]byte(rawKey))
	hashedKey := hex.EncodeToString(hashedKeyBytes[:])
	if hashedKey != siteInfo.UploadSecretHash {
		w.WriteHeader(401)
		log.Printf("Got invalid upload request with build key '%s'", hashedKey)
		return
	}

	if req.Method != "POST" {
		w.WriteHeader(405) // Method not allowed
		return
	}

	// Parse the body as JSON
	bodyData, err := ioutil.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(400)
		_, _ = w.Write([]byte("Failed to read body"))
		log.Printf("Failed to read webhook body: %s", err)
		return
	}

	var body AppveyorNotification

	err = json.Unmarshal(bodyData, &body)
	if err != nil {
		w.WriteHeader(400)
		log.Printf("Failed to parse webhook body: '%s' with body: '%s'", err, string(bodyData))
		return
	}

	// Validate stuff
	// TODO Validate the repository provider
	// TODO Validate the repository name

	commitId, err := extendCommitId(body.EventData.CommitId, true)
	if err != nil {
		w.WriteHeader(400)
		log.Printf("Invalid commit ID '%s' (error %s) for request: '%s'", body.EventData.CommitId, err, string(bodyData))
		return
	}

	// Make a new file for this event
	path := fmt.Sprintf("%s/appveyor-%s-%d-%d.ar", BUILDS_DIR, commitId, body.EventData.BuildId, body.EventData.BuildNumber)

	if _, err := os.Stat(path); !errors.Is(err, os.ErrNotExist) {
		w.WriteHeader(400)
		log.Printf("Duplicate file path '%s' (stat err: %s) for request: '%s'", path, err, string(bodyData))
		return
	}

	// Open the file and start writing the archive
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0666)
	if err != nil {
		log.Printf("Failed to open archive file: %s", err)
		w.WriteHeader(500)
		return
	}

	// Actually download the artefacts in another goroutine, to avoid holding up the server.
	go func() {
		log.Printf("Downloading archive %s ...", path)
		err := buildArchiveFile(file, body, commitId, bodyData)
		if err == nil {
			log.Printf("Done downloading archive %s", path)
			globalBuilds.AddBuild(path)
		} else {
			log.Printf("Error downloading archive %s: %s", path, err)
		}
	}()

	w.WriteHeader(200)
}

func buildArchiveFile(file *os.File, body AppveyorNotification, commitId string, bodyData []byte) error {
	var err error

	writer := ar.NewWriter(file)
	err = writer.WriteGlobalHeader()
	if err != nil {
		return fmt.Errorf("Failed to write global archive header: %w", err)
	}

	// Write the request file
	// Do this first so we have it for diagnostics if something later fails
	err = writeArFile(file, writer, "appveyor.json", bodyData)
	if err != nil {
		return fmt.Errorf("Failed to write archive appveyor JSON: %w", err)
	}

	// Build our specification JSON, planning out the artefacts we're going
	// to download. We should write the long-filenames file before downloading
	// any artefacts, so the archive is still readable if one of the downloads
	// fails.
	info := &BuildInfo{
		// The BuildId should be unique, while we can reset the build number
		BuildId:    body.EventData.BuildId,
		Commit:     commitId,
		Successful: body.EventName == "build_success",
	}

	info.Date, err = time.Parse(APPVEYOR_TIMESTAMP_FORMAT, body.EventData.Started)
	if err != nil {
		return fmt.Errorf("failed to parse started timestamp: %w", err)
	}

	type DownloadJob struct {
		URL        string
		OutputName string
		NameOffset int
	}
	var downloadJobs []*DownloadJob
	for _, job := range body.EventData.Jobs {
		jobName := job.Name
		if jobName == "" {
			jobName = "unnamed"
		}

		for _, artefact := range job.Artefacts {
			// Put the job name in to avoid filename conflicts
			arName := jobName + "/" + artefact.FileName

			// AR doesn't support strokes in filenames, so use :: instead
			arName = strings.ReplaceAll(arName, "/", "::")

			// Make up a unique ID for use in the download URLs
			var idBytes [12]byte // Multiples of 3 bytes doesn't need padding or waste space
			_, err := rand.Read(idBytes[:])
			if err != nil {
				return fmt.Errorf("could not read random data for artefact ID: %w", err)
			}
			uniqueId := base64.URLEncoding.EncodeToString(idBytes[:])

			downloadJobs = append(downloadJobs, &DownloadJob{
				URL:        artefact.URL,
				OutputName: arName,
			})
			info.Artefacts = append(info.Artefacts, &Artefact{
				JobId:            job.Id,
				AppveyorFilename: artefact.FileName,
				ArchiveFilename:  arName,
				UniqueID:         uniqueId,
			})
		}
	}

	// Write the info file
	infoData, err := json.Marshal(&info)
	if err != nil {
		return fmt.Errorf("Failed to marshal archive info JSON: %w", err)
	}
	err = writeArFile(file, writer, "info.json", infoData)
	if err != nil {
		return fmt.Errorf("Failed to write archive info JSON: %w", err)
	}

	// Write the long filenames file, if required
	var longFilenameData string
	for _, job := range downloadJobs {
		job.NameOffset = len(longFilenameData)
		longFilenameData += job.OutputName + "/\n"
	}
	if len(longFilenameData) != 0 {
		err = writeArFile(file, writer, "//", []byte(longFilenameData))
		if err != nil {
			return fmt.Errorf("Failed to write archive long filename data: %w", err)
		}
	}

	httpClient := &http.Client{
		Timeout: 90 * time.Second,
	}

	// Don't leave keep-alive connections around, we're not going
	// to use this client again.
	defer httpClient.CloseIdleConnections()

	// Download and append the artefacts
	for _, job := range downloadJobs {
		// Read the artefact into memory - we're only doing one at a time so this should be fine.
		// It *would* be possible to avoid the read-to-memory step, but a few megabytes of RAM
		// isn't a problem.

		resp, err := httpClient.Get(job.URL)
		if err != nil {
			return fmt.Errorf("Failed to download (start) artefact '%s': %w", job.URL, err)
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			return fmt.Errorf("failed to download build artefact '%s': %d %s",
				job.OutputName, resp.StatusCode, resp.Status)
		}

		bodyData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("Failed to download (read) artefact '%s': %w", job.URL, err)
		}

		name := fmt.Sprintf("/%d", job.NameOffset)
		err = writeArFile(file, writer, name, bodyData)
		if err != nil {
			return fmt.Errorf("Failed to write archive artefact (path: '%s') data: %w", job.OutputName, err)
		}
	}

	return nil
}

func writeArFile(output io.Writer, writer *ar.Writer, name string, data []byte) error {
	err := writer.WriteHeader(&ar.Header{
		Name:    name,
		ModTime: time.Now(),
		Mode:    0666, // Everyone gets RW
		Size:    int64(len(data)),
	})
	if err != nil {
		return fmt.Errorf("failed to write AR file header: %w", err)
	}

	// AR archive files must be padded to even lengths with newlines
	if len(data)%2 == 1 {
		data = append(data, '\n')
	}

	_, err = output.Write(data)
	if err != nil {
		return fmt.Errorf("failed to write archive appveyor JSON header: %w", err)
	}

	return nil
}

// AppVeyor passes us truncated commit IDs. This finds a commit with the
// matching ID in the repository, and returns it in full form.
func extendCommitId(shortId string, updateRepo bool) (string, error) {
	// Require a sane commit ID length - between 8 and 40 charactors.
	// Git defaults to 10.
	if len(shortId) < 8 || len(shortId) > 40 {
		return "<invalid>", fmt.Errorf("invalid commit id length for '%s'", shortId)
	}

	repo, err := getCachedRepo()
	if err != nil {
		return "<invalid>", fmt.Errorf("could not get repo to extend commit id: %w", err)
	}

	// Look through all the commits, in case this commit was removed from a branch.
	// If that happens it won't show up in the UI, but we should still download it.
	var found *Commit
	for _, commit := range repo.AllCommits {
		if !strings.HasPrefix(commit.Hash, shortId) {
			continue
		}

		if found == nil {
			found = commit
			continue
		}

		// We found a duplicate commit!
		return "<invalid>", fmt.Errorf("ambiguous short commit ID '%s': commits '%s' and '%s' both match", shortId, found.Hash, commit.Hash)
	}

	if found == nil {
		// Maybe there's a new commit on the server?
		if updateRepo {
			err = fetchRepo()
			if err != nil {
				return "<invalid>", fmt.Errorf("failed to update repo (commit not found locally): %w", err)
			}
			return extendCommitId(shortId, false)
		}

		return "<invalid>", fmt.Errorf("short commit ID '%s' doesn't have any matches in the repo", shortId)
	}

	return found.Hash, nil
}

//// Build reading support

type BuildCollection struct {
	ByCommit map[string][]*BuildReadInfo
	lock     sync.Mutex
}

type BuildReadInfo struct {
	ArchivePath string
	Info        *BuildInfo
	AppVeyor    *AppveyorNotification
	Files       map[string]*SeekInfo
}

type SeekInfo struct {
	StartOffset int64
	Size        int64
}

func NewBuildCollection() (*BuildCollection, error) {
	self := &BuildCollection{
		ByCommit: make(map[string][]*BuildReadInfo),
	}

	files, err := ioutil.ReadDir(BUILDS_DIR)
	if err != nil {
		return nil, fmt.Errorf("could not list builds dir: %w", err)
	}

	for _, fileInfo := range files {
		if !strings.HasSuffix(fileInfo.Name(), ".ar") {
			continue
		}

		path := BUILDS_DIR + "/" + fileInfo.Name()

		// Don't abort here, since we should still read all
		// the other files.
		err = self.AddBuild(path)
		if err != nil {
			log.Printf("Couldn't add build file '%s' during scan: %s", path, err)
		}
	}

	return self, nil
}

func (bc *BuildCollection) AddBuild(path string) error {
	bc.lock.Lock()
	defer bc.lock.Unlock()

	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("couldn't open build file: %w", err)
	}

	readInfo, err := readArchive(file)
	if err != nil {
		return fmt.Errorf("couldn't read build file: %w", err)
	}
	readInfo.ArchivePath = path

	commit := readInfo.Info.Commit
	bc.ByCommit[commit] = append(bc.ByCommit[commit], readInfo)
	return nil
}

func readArchive(file *os.File) (*BuildReadInfo, error) {
	br := &BuildReadInfo{
		Files: make(map[string]*SeekInfo),
	}

	reader := ar.NewReader(file)

	var stringTable []byte

	for {
		header, err := reader.Next()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			return nil, fmt.Errorf("failed to read archive header: %w", err)
		}

		if header.Name == "//" {
			stringTable = make([]byte, header.Size)
			_, err = reader.Read(stringTable)
			if err != nil {
				return nil, fmt.Errorf("failed to read string table: %w", err)
			}
			continue
		}

		// Check if this is a long name - then it's in the form of /1234 where 1234 is the
		// byte offset into the string table for the start of the the filename.
		var fileName = header.Name
		var nameOffset int
		_, err = fmt.Sscanf(header.Name, "/%d", &nameOffset)
		if err == nil {
			// Filenames are terminated with a stroke then a newline, so look for the stroke.
			part := stringTable[nameOffset:]
			length := bytes.IndexByte(part, '/')

			if length == -1 {
				return nil, fmt.Errorf("failed to find string table terminator for filename '%s'", header.Name)
			}

			fileName = string(part[:length])
		}

		fileOffset, err := file.Seek(0, io.SeekCurrent)
		if err != nil {
			return nil, fmt.Errorf("failed to tell file position: %w", err)
		}

		br.Files[fileName] = &SeekInfo{
			StartOffset: fileOffset,
			Size:        header.Size,
		}

		if fileName == "info.json" {
			fileData := make([]byte, header.Size)
			_, err = reader.Read(fileData)
			if err != nil {
				return nil, fmt.Errorf("failed to read info.json: %w", err)
			}

			info := &BuildInfo{}
			err = json.Unmarshal(fileData, info)
			if err != nil {
				return nil, fmt.Errorf("failed to unmarshal info.json: %w", err)
			}
			br.Info = info
			continue
		}

		if fileName == "appveyor.json" {
			fileData := make([]byte, header.Size)
			_, err = reader.Read(fileData)
			if err != nil {
				return nil, fmt.Errorf("failed to read appveyor.json: %w", err)
			}

			av := &AppveyorNotification{}
			err = json.Unmarshal(fileData, av)
			if err != nil {
				return nil, fmt.Errorf("failed to unmarshal appveyor.json: %w", err)
			}
			br.AppVeyor = av
			continue
		}
	}

	if br.Info == nil {
		return nil, fmt.Errorf("could not find info.json")
	}
	if br.AppVeyor == nil {
		return nil, fmt.Errorf("could not find appveyor.json")
	}

	return br, nil
}
