package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
)

// var rootTemplate *template.Template

type Commit struct {
	Hash         string
	Ref          *object.Commit `json:"-"`
	MainParent   string
	OtherParents []string
	Branches     []*Branch

	Children []*Commit `json:"-"`
}

type Branch struct {
	Name string
	Ref  *plumbing.Reference
}

type VisualCommit struct {
	Column       *Column `json:"-"`
	Y            int
	ParentColumn *Column `json:"-"`
	ParentY      int
	Index        int

	Summary string
	Commit  *Commit

	// This commit's index in Column.commits.
	ColumnPos int

	// If true, the summary will be hidden - this is required if two
	// commits are shown in the row (due to being identical and likely
	// just rebased across).
	HideDuplicateText bool

	// Does this commit have any builds associated with it? If so, the UI
	// needs to display a link to it.
	HasBuilds bool
}

type Column struct {
	X      int
	SlotId int

	FirstIdx, LastIdx int

	// The number of commits away the latest commit in this branch is from
	// a commit with no children where that child has this commit listed as
	// the first parent.
	// Basically, this means that branches and commits that are merged back
	// in have a distance of zero. The exception is that we can increase this
	// for important branches, to make them seem more important to the layout.
	// This is used for arranging columns.
	Distance int

	Id int

	Commits []*VisualCommit
}

type MergeLine struct {
	SrcCol *Column
	DstCol *Column
	SrcY   int
	DstY   int
}

type RepoInfo struct {
	Branches []*Branch
	Commits  []*Commit
	ById     map[string]*Commit

	// Usually, commits which aren't accessable are excluded to avoid
	// mucking up the graph stuff. This contains an exhaustive list of
	// all the repo's commits, whether or not they're accessable.
	AllCommits []*Commit
}

var repoCache *RepoInfo
var repoCacheLock sync.Mutex

func loadRepo() (*RepoInfo, error) {
	info := &RepoInfo{
		ById: make(map[string]*Commit),
	}

	var err error

	repo, err := git.PlainOpen(siteInfo.RepoPath)
	if err != nil {
		return nil, fmt.Errorf("Failed to open repo: %w", err)
	}

	branchById := make(map[string][]*Branch)

	branchIter, err := repo.References()
	if err != nil {
		return nil, fmt.Errorf("Failed to read repo branches: %e", err)
	}
	defer branchIter.Close()
	err = branchIter.ForEach(func(ref *plumbing.Reference) error {
		// Find out if this is a reference on the 'origin' remote, and
		// if so filter out HEAD.
		name := ref.Name().String()

		prefix := "refs/remotes/origin/"
		if !strings.HasPrefix(name, prefix) {
			return nil
		}
		name = strings.TrimPrefix(name, prefix)
		if name == "HEAD" {
			return nil
		}

		branch := &Branch{
			Name: name,
			Ref:  ref,
		}
		info.Branches = append(info.Branches, branch)

		// Add this branch to the list of branches that are on this commit
		currentBranches, _ := branchById[ref.Hash().String()]
		branchById[ref.Hash().String()] = append(currentBranches, branch)

		log.Printf("branch: %s", name)
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("Failed to iterate repo branches: %e", err)
	}

	// Use Log to walk through all the commits
	logIter, err := repo.Log(&git.LogOptions{
		All: true,
	})
	if err != nil {
		return nil, fmt.Errorf("Failed to read git log: %e", err)
	}

	err = logIter.ForEach(func(commit *object.Commit) error {
		branches, _ := branchById[commit.Hash.String()]

		ourCommit := &Commit{
			Ref:  commit,
			Hash: commit.Hash.String(),
		}
		if len(commit.ParentHashes) != 0 {
			// Note the first commit doesn't have any parents
			ourCommit.MainParent = commit.ParentHashes[0].String()
		}
		for i := 1; i < len(commit.ParentHashes); i++ {
			ourCommit.OtherParents = append(ourCommit.OtherParents, commit.ParentHashes[i].String())
		}
		info.AllCommits = append(info.AllCommits, ourCommit)
		info.ById[ourCommit.Hash] = ourCommit

		for _, branch := range branches {
			ourCommit.Branches = append(ourCommit.Branches, branch)
		}

		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("Failed to iterate repo branches: %e", err)
	}

	// Remove all the commits that aren't reachable via the branches
	// This is effectively a GC pass, and is required if a branch was
	// rolled back via a force-push. If we didn't do this, these
	// branches wouldn't be addeed to the sorted list as they still
	// have children.
	info.Commits = runCommitGC(info.Branches, info.ById)

	// Mark all the commit children
	for _, commit := range info.Commits {
		addChild := func(parentHash string) {
			parent := info.ById[parentHash]
			parent.Children = append(parent.Children, commit)
		}

		if commit.MainParent != "" { // An empty string denotes the root commit
			addChild(commit.MainParent)
		}
		for _, parent := range commit.OtherParents {
			addChild(parent)
		}
	}

	return info, nil
}

func getCachedRepo() (*RepoInfo, error) {
	// Setting this disables the cache for development purposes
	if os.Getenv("BT_DISABLE_REPO_CACHE") == "1" {
		return loadRepo()
	}

	repoCacheLock.Lock()
	defer repoCacheLock.Unlock()

	if repoCache != nil {
		return repoCache, nil
	}

	info, err := loadRepo()
	if err != nil {
		return nil, err
	}

	repoCache = info
	return repoCache, nil
}

func fetchRepo() error {
	// While it's a little bit weird, the cache lock makes sense: we
	// probably shouldn't be fetching the repo while we're also
	// reading and caching it.
	// We certainly need *a* lock to prevent two concurrent fetches
	// though, so reusing it seems reasonable.
	repoCacheLock.Lock()
	defer repoCacheLock.Unlock()

	// Invalidate the cache
	repoCache = nil

	log.Printf("Updating repo...")

	repo, err := git.PlainOpen(siteInfo.RepoPath)
	if err != nil {
		return fmt.Errorf("failed to open (to fetch) repo: %w", err)
	}

	err = repo.Fetch(&git.FetchOptions{
		// Don't break if upstream force-pushes'
		Force: true,
	})

	if err != nil && err != git.NoErrAlreadyUpToDate {
		log.Printf("Repo update failed: %s", err)
		return fmt.Errorf("failed to fetch repo: %w", err)
	}

	log.Printf("Repo update complete")

	return nil
}

func handleRoot(w http.ResponseWriter, req *http.Request) {
	var err error

	if req.URL.Path != "/" {
		http.NotFound(w, req)
		return
	}

	// Reload on serve for debugging
	rootTemplate := template.Must(template.ParseFiles("template/index.html"))

	var ctx struct {
		SiteInfo   *SiteInfo
		Commits    []*VisualCommit
		Height     int
		MergeLines []MergeLine
	}
	ctx.SiteInfo = siteInfo

	info, err := getCachedRepo()
	if err != nil {
		log.Printf("Failed to load repo: %s", err)
		return
	}

	// The commits that are suitable for placing in the next row of history (which runs newest
	// to oldest). A commit is suitable if it has no children and has a path to a branch.
	nextCommits := make(map[*Commit]bool)

	// Mark all the initial next-commit candidates: the commits denoted by branches that
	// have no children.
	for _, branch := range info.Branches {
		commit := info.ById[branch.Ref.Hash().String()]

		if len(commit.Children) == 0 {
			nextCommits[commit] = true
		}
	}

	// Sort the commits into a linear history
	var sorted []*Commit
	placed := make(map[*Commit]bool)
	for len(nextCommits) != 0 {
		var newest *Commit

		for available := range nextCommits {
			// If this is the first commit, select it
			if newest == nil {
				newest = available
				continue
			}

			// If this commit is newer than the previous newest one, use it
			newestWhen := newest.Ref.Author.When
			availWhen := available.Ref.Author.When
			if newestWhen.Before(availWhen) {
				newest = available
				continue
			}

			// If it has the same date (likely because it was rebased between
			// branches), sort by the commit IDs so the ordering is stable and
			// doesn't shuffle around between refreshes.
			if newestWhen.Equal(availWhen) &&
				strings.Compare(newest.Hash, available.Hash) == 1 {

				newest = available
				continue
			}
		}

		if newest == nil {
			panic(fmt.Sprintf("Found null newest commit, with the remaining commits: %v", nextCommits))
		}

		delete(nextCommits, newest)

		sorted = append(sorted, newest)
		placed[newest] = true

		markAvailable := func(hash string) {
			commit := info.ById[hash]

			if commit == nil {
				panic(fmt.Sprintf("Invalid null commit for ID '%s'", hash))
			}

			// Check this commit hasn't already been placed
			if _, used := placed[commit]; used {
				return
			}

			// Check that all of it's children have been placed
			for _, child := range commit.Children {
				if _, ready := placed[child]; !ready {
					return
				}
			}

			nextCommits[commit] = true
		}

		if newest.MainParent != "" {
			markAvailable(newest.MainParent)
		}
		for _, parent := range newest.OtherParents {
			markAvailable(parent)
		}
	}

	visByCommit := make(map[*Commit]*VisualCommit)

	var nextY = 0

	// Build visual representations for the commits, and sort them into columns. A column is a
	// sequence of commits, where each commit inside the column (excluding at each end) has only
	// one parent and one child. The first commit can have multiple parents, and the last can
	// have multiple children.
	var columns []*Column

	for i, commit := range sorted {
		//  Testing
		// log.Printf("Commit: %s  %s", commit.Hash, commit.Ref.Author.When.String())
		// for _, branch := range commit.Branches {
		// 	log.Printf("\t(br: %s)", branch.Name)
		// }

		// If this commit has one non-merge child commit, use the same X so they line up
		var column *Column
		if len(commit.Children) == 1 && len(commit.Children[0].OtherParents) == 0 {
			childVis := visByCommit[commit.Children[0]]
			column = childVis.Column
		} else {
			// Set the first index to the lowest index of the children, to
			// reserve space for the line running to them.
			// Also, calculate our distance here at the same time.
			var lowestId = i
			var distance = 0
			for _, child := range commit.Children {
				vis := visByCommit[child]
				reserveIdx := vis.Index + 1 // +1 so we can still line up with it
				if reserveIdx < lowestId {
					lowestId = reserveIdx
				}

				childDist := vis.Column.Distance + vis.ColumnPos
				if childDist > distance {
					distance = childDist
				}
			}

			column = &Column{
				FirstIdx: lowestId,
				Distance: distance,
				Id:       len(columns),
			}
			columns = append(columns, column)
		}
		column.LastIdx = i

		// Build the summary, noting any branches
		summary := strings.SplitN(commit.Ref.Message, "\n", 2)[0]
		for _, branch := range commit.Branches {
			summary = fmt.Sprintf("[%s] %s", branch.Name, summary)
		}

		// Move down, unless this commit shares the timestamp and message with the previous one, in which
		// case assume it was rebased over and merge them to the same row.
		var merge bool
		var y int
		if i > 0 {
			prev := visByCommit[sorted[i-1]]
			if prev.Summary == summary && prev.Commit.Ref.Author.When.Equal(commit.Ref.Author.When) {
				merge = true
				y = prev.Y
				prev.HideDuplicateText = true
			}
		}
		if !merge {
			y = nextY + 30
			nextY += 30
		}

		// Make sure the SVG can fit everything
		minHeight := y + 35
		if minHeight > ctx.Height {
			ctx.Height = minHeight
		}

		// Create the SVG node
		vis := &VisualCommit{
			Commit:    commit,
			Summary:   summary,
			Column:    column,
			ColumnPos: len(column.Commits),
			Y:         y,
			Index:     i,
		}
		ctx.Commits = append(ctx.Commits, vis)
		visByCommit[commit] = vis
		column.Commits = append(column.Commits, vis)

		// Check if there's any builds associated with this commit
		if builds := globalBuilds.ByCommit[commit.Hash]; len(builds) != 0 {
			vis.HasBuilds = true
		}
	}

	// This will join columns together, so our columns array is now invalid.
	assignColumns(columns, visByCommit, info.ById)
	columns = nil // Nil it out to prevent hard-to-find bugs from using it.

	// Fix up all the parent coordinates, now all the nodes are available
	for commit, vis := range visByCommit {
		if commit.MainParent == "" {
			// Root node, hide the parent line by making it start and
			// end at the same point.
			vis.ParentColumn = vis.Column
			vis.ParentY = vis.Y
			continue
		}
		parentVis := visByCommit[info.ById[commit.MainParent]]

		vis.ParentColumn = parentVis.Column
		vis.ParentY = parentVis.Y

		// Create the SVG stuff for the additional parent lines running to merge commits
		for _, parentHash := range commit.OtherParents {
			pv := visByCommit[info.ById[parentHash]]
			ctx.MergeLines = append(ctx.MergeLines, MergeLine{
				SrcCol: pv.Column,
				DstCol: vis.Column,
				SrcY:   pv.Y,
				DstY:   vis.Y,
			})
		}
	}

	err = rootTemplate.Execute(w, &ctx)
	if err != nil {
		log.Printf("Failed to serve root page template: %e", err)
	}
}

func runCommitGC(branches []*Branch, byId map[string]*Commit) []*Commit {
	// The set of commits that we've scanned
	scanned := make(map[*Commit]bool)

	// Since the vast majority of commits have one parent, we can perform
	// the GC scan using recursion.
	var scan func(*Commit)
	scan = func(commit *Commit) {
		// Walk the parent chain, triggering recursion for merges
		for {
			if _, found := scanned[commit]; found {
				return
			}

			scanned[commit] = true

			for _, parent := range commit.OtherParents {
				scan(byId[parent])
			}

			if commit.MainParent == "" {
				// Root commit
				return
			}

			commit = byId[commit.MainParent]
		}
	}

	for _, branch := range branches {
		commit := byId[branch.Ref.Hash().String()]

		// If the repo is broken, the commit might be nil.
		if commit == nil {
			log.Printf("Missing commit for branch '%s'", branch.Name)
			continue
		}

		scan(commit)
	}

	// Build the new list of reachable commits
	var commits []*Commit
	for commit := range scanned {
		commits = append(commits, commit)
	}
	return commits
}

func assignColumns(columns []*Column, visByCommit map[*Commit]*VisualCommit, byId map[string]*Commit) {
	// Combine columns together, based on the first parent.

	// Sort the columns to place the last ones first, so that when we join them we're
	// doing so a bit more accurately (otherwise a large number of small blocks could
	// be ignored in favour of a smaller number of large blocks).
	sort.SliceStable(columns, func(a, b int) bool {
		return columns[a].LastIdx > columns[b].LastIdx
	})

	tmpReAdd := make(map[*Column]bool)
	for _, column := range columns {
		tmpReAdd[column] = true
	}

	for _, column := range columns {
		// Merge commits into their newest child, so long as they are that
		// child's first parent. This is for merge commtis, to make the main side show
		// up cleanly.
		// We need to always use the newest child, since that means there's a gap that
		// the inhertance lines will fit in.

		head := column.Commits[0].Commit

		var next *VisualCommit
		for _, commit := range head.Children {
			vis := visByCommit[commit]
			if next == nil || vis.Index < next.Index {
				next = vis
			}
		}

		// No children?
		if next == nil {
			continue
		}

		if next.Commit.MainParent != head.Hash {
			continue
		}

		nextCol := next.Column

		// Make sure this commit we've found is at the end of it's block
		if nextCol.Commits[len(nextCol.Commits)-1] != next {
			continue
		}

		// Check the columns only overlap at the one point where their commits
		// meet, which is expected due to reserving the space for the line running
		// from the parent to the child.
		if nextCol.LastIdx > column.FirstIdx {
			continue
		}

		// Merge this column into the next one
		nextCol.Commits = append(nextCol.Commits, column.Commits...)
		nextCol.LastIdx = column.LastIdx

		for _, commit := range column.Commits {
			commit.Column = nextCol
		}

		delete(tmpReAdd, column)
	}

	for _, column := range columns {
		// Check this column hasn't been removed
		if _, ok := tmpReAdd[column]; !ok {
			continue
		}

		last := column.Commits[len(column.Commits)-1].Commit
		if last.MainParent == "" {
			continue // Root commit
		}

		parent := byId[last.MainParent]
		parentVis := visByCommit[parent]

		// Make sure it's safe to merge with this column
		if parentVis.Column.Commits[0] != parentVis {
			continue
		}

		// delete(tmpReAdd, column)
	}

	columns = nil
	for column := range tmpReAdd {
		columns = append(columns, column)
	}

	// Sort the columns, so we can keep track of which 'slots' (X positions) are available
	// and mark them as clear when we reach a column whose FirstIdx exceeds their LastIdx.
	sort.SliceStable(columns, func(a, b int) bool {
		return columns[a].FirstIdx < columns[b].FirstIdx
	})

	var slots []*Column

	for _, column := range columns {
		// Clear out all the slot cells that are now free. Also find the first
		// available index for this column.
		var nextIdx = -1
		for i, col := range slots {
			if col != nil && col.LastIdx < column.FirstIdx {
				slots[i] = nil
			}
			if slots[i] == nil && nextIdx == -1 {
				nextIdx = i
			}
		}

		if nextIdx == -1 {
			// No free places, add a new slot
			nextIdx = len(slots)
			slots = append(slots, column)
		} else {
			slots[nextIdx] = column
		}
		column.SlotId = nextIdx
	}

	// Assign the X positions based on the slot IDs
	for _, column := range columns {
		column.X = 30 + column.SlotId*30
	}
}
